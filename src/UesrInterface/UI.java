package UesrInterface;

import Adapter.Adapter;
import Adapter.FirstFitAdapter;
import Adapter.NextFitAdapter;
import Core.MemoryBlock;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import static UesrInterface.CommandType.ADD;
import static UesrInterface.CommandType.REMOVE;

public class UI extends JFrame
{
    private Container rootCon = new Container();
    private Container canvasCon = new Container();
    private Integer canvasHeight = 400;

    private JComboBox<String> modeChooseOptions = new JComboBox<>();
    private JButton stepButton = new JButton("Next Step");
    private JButton autoButton = new JButton("Automatically");
    private JButton applyButton = new JButton("Apply");
    private JButton reset = new JButton("Reset");
    private TextArea board = new TextArea();

    private JLabel descriptionLabel = new JLabel("Memory Size: ");
    private JTextField descriptionTextField = new JTextField("",10);
    private JLabel typeLabel = new JLabel("Operation: ");
    private JComboBox<String> typeComboBox = new JComboBox<>();


    private FirstFitAdapter ffAdapter = new FirstFitAdapter(this);
    private NextFitAdapter nfAdapter = new NextFitAdapter(this);
    private Adapter nowAdapter = ffAdapter; // Default adapter is ffAdapter.

    private ArrayList<Command> commands;
    private int commandArrayListCnt = 0;

    private static String helpMessage =
            "<html>" +
            "<body>" +
            "<h1>操作系统课程设计</h1>" +
            "<h2>基础信息</h2>" +
            "<ul><li><b>题目：</b>基于空闲分区链的首次适应算法的模拟实现</li>" +
            "    <li><b>小组成员：</b>20167888、20167924、20168066、20168180</li>" +
            "    <li><b>班级：</b>计科1601班</li></ul>" +
            "<h2>基本算法</h2>" +
            "    <h3> 首次适配算法（First Fit，FF）</h3>" +
            "<ul><li>从链首开始顺序查找，直至找到满足要求的内存块</li> <li>释放内存时合并空闲内存块</li> </ul>" +
            "    <h3>循环首次适配算法（Next Fit，NF）</h3>" +
            "<ul><li>从上一次查找的空闲区域的下一块开始顺序查找，直至找到满足要求的内存块</li> <li>释放内存时合并空闲内存块</li> </ul>" +
            "<h2>操作说明</h2>" +
            "<ul><li>顶端菜单可以选择算法</li> <li>中间示意框显示操作指令与结果</li> " +
            "    <li>“Reset”按钮可重新初始化整个流程</li> <li>“Automatically”按钮可以自动演示所有操作</li>" +
            "    <li>“Next Step”操作可逐步运行指令</li>" +
            "    <li>底部将图示内存块状况，白色为空闲内存块</li> </ul>" +
            "</body>" +
            "</html>";

    public UI()
    {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); // Now the program should quit after closing the window

        // Help initialization
        JOptionPane.showMessageDialog(null,
                helpMessage,
                "操作系统课程设计",
                JOptionPane.INFORMATION_MESSAGE);

        setTitle("操作系统课程设计——Northeastern University at Qinhuangdao");

        // Root Layout
        GridBagLayout rootLayout = new GridBagLayout();
        rootCon.setLayout(rootLayout);

        // Tasks Initialization
        commands = new ArrayList<>();
        setModeChooseOptions(rootCon);
        setBoard(rootCon);
        setCommands(rootCon);
        setApplyButton(rootCon);
        setAutoButton(rootCon);
        setStepButton(rootCon);
        setReset(rootCon);
        canvasCon.setSize(1200, canvasHeight);
        canvasCon.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
        rootCon.add(canvasCon);
        setGridLayoutConstraints(rootLayout);

        setContentPane(rootCon);
        setSize(1200, 700);
        setResizable(false);
        setVisible(true);


    }

    private void setApplyButton(Container rootCon)
    {
        applyButton.addActionListener(e ->
        {
            try
            {
                int receiveNumber = Integer.valueOf(descriptionTextField.getText());
                if(typeComboBox.getSelectedItem().toString().equals("Add"))
                {
                    if(receiveNumber > nowAdapter.table.getLast().getEnd() || receiveNumber <= 0)
                    {
                        throw new RuntimeException();
                    }
                    commands.add(new Command(ADD, receiveNumber));
                }
                else
                {
                    if(nowAdapter.tags.size() < receiveNumber || receiveNumber < 0 || nowAdapter.tags.get(receiveNumber)== -1 )
                    {
                        throw new RuntimeException();
                    }
                    commands.add(new Command(REMOVE, receiveNumber));
                }
            }
            catch(NumberFormatException err)
            {
                JOptionPane.showMessageDialog(this,
                        "Bad input: Please input a number.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
            catch(NullPointerException err)
            {
                JOptionPane.showMessageDialog(this,
                        "You are lucky! This is an impossible error, but you got it!",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
            catch (RuntimeException err)
            {
                JOptionPane.showMessageDialog(this,
                        "Bad input: invalid number or non-existed task.",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        });
    }

    private void setReset(Container con)
    {
        UI nowUI=this;
        reset.setOpaque(true);
        reset.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        reset.setBackground(Color.RED);
        reset.addActionListener(e ->
        {
            this.canvasCon.removeAll();
            commandArrayListCnt = 0;
            ffAdapter = new FirstFitAdapter(nowUI);
            nfAdapter = new NextFitAdapter(nowUI);
            appendBoard("Reset Complete!\n");
        });
        con.add(reset);
    }

    private void setBoard(Container con)
    {
        board.setEditable(false);
        board.setColumns(30);
        board.setRows(8);
        board.setFont(new Font("Noto Serif SC",Font.BOLD,30));
        board.append("Default option is First Fit.\n");
        con.add(board);
    }

    private void setModeChooseOptions(Container container)
    {
        UI nowUI = this;
        modeChooseOptions.setOpaque(true);
        modeChooseOptions.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        modeChooseOptions.addItem("First Fit");
        modeChooseOptions.addItem("Next Fit");
        modeChooseOptions.addItemListener(e ->
        {
            if (ItemEvent.SELECTED == e.getStateChange())
            {
                commandArrayListCnt = 0;
                board.append("You choose: " + e.getItem().toString() + "\n");
                // select first
                if(e.getItem().toString().equals("First Fit"))
                {
                    ffAdapter = new FirstFitAdapter(nowUI);
                    nowAdapter = ffAdapter;
                }
                // select next
                else if(e.getItem().toString().equals("Next Fit"))
                {
                    nfAdapter = new NextFitAdapter(nowUI);
                    nowAdapter = nfAdapter;
                }
                canvasCon.invalidate();
                canvasCon.removeAll();
                canvasCon.validate();
            }
        });
        container.add(modeChooseOptions);
    }

    private void setCommands(Container container)
    {
        descriptionLabel.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        descriptionTextField.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        typeComboBox.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        typeLabel.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        typeComboBox.addItem("Add");
        typeComboBox.addItem("Remove");
        typeComboBox.addItemListener(e ->
        {
           if(ItemEvent.SELECTED == e.getStateChange())
           {
               if(e.getItem().toString().equals("Add"))
                   descriptionLabel.setText("Memory Size: ");
               else
                   descriptionLabel.setText("Tag Number: ");
           }
        });

        container.add(descriptionLabel);
        container.add(descriptionTextField);
        container.add(applyButton);
        container.add(typeLabel);
        container.add(typeComboBox);

        // TODO: Work of Task 1, Task 2, and Task 3
        commands.add(new Command(ADD, 130));
        commands.add(new Command(ADD, 60));
        commands.add(new Command(ADD, 100));
        commands.add(new Command(REMOVE, 2));
        commands.add(new Command(ADD, 200));
        commands.add(new Command(REMOVE , 3));
        commands.add(new Command(REMOVE, 1));
        commands.add(new Command(ADD, 140));
        commands.add(new Command(ADD, 60));
        commands.add(new Command(ADD, 50));
        commands.add(new Command(REMOVE, 6));
    }
    private void setAutoButton(Container container)
    {
        UI nowUI = this;
        // Automatically Presentation
        autoButton.setOpaque(true);
        autoButton.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        autoButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                board.append("Automatic presentation begins.\n");
                // FIXME: quit the thread if the window is closed
                Thread t = new Thread()
                {
                    @Override
                    public void run()
                    {
                        super.run();
                        ffAdapter = new FirstFitAdapter(nowUI);
                        nfAdapter = new NextFitAdapter(nowUI);
                        for (int i = 0; i < commands.size() + 1; i++) // yes, add another clicks
                        {
                            stepButton.doClick();
                            try
                            {
                                Thread.sleep(1250);
                            }
                            catch (InterruptedException err)
                            {
                                err.printStackTrace();
                            }
                        }
                    }
                };
                t.start();

            }
        });
        container.add(autoButton);
    }

    public void appendBoard(String str)
    {
        board.append(str);
    }

    private void setStepButton(Container container)
    {
        UI nowUI = this;
        stepButton.setOpaque(true);
        stepButton.setFont(new Font("Noto Sans SC",Font.PLAIN,15));
        stepButton.addActionListener(e ->
        {
            canvasCon.invalidate();
            canvasCon.removeAll();
            Adapter adapter = (modeChooseOptions.getSelectedIndex() == 0)? ffAdapter : nfAdapter;
            try
            {
                Command current = commands.get(commandArrayListCnt);
                String operation = current.getState()?"allocates":"releases";
                board.append("Job " + current.getTag() + ": " + operation + " " + current.getLength() + " memory.\n");
                commandArrayListCnt++;
                if (current.getState())
                    adapter.insert(current.getLength(), current.getTag());
                else
                    adapter.release(current.getTag());
                for(MemoryBlock mBlk: adapter.table)
                {
                    System.out.println(mBlk.getStart()+" "+mBlk.getEnd());
                    canvasCon.add(new MyCanvas(mBlk.getStart(), mBlk.getLength(), canvasHeight, mBlk.getTag(), mBlk.getMemoryState()));
                }
                System.out.println("\n");
            }
            catch (RuntimeException err)
            {
                board.append("Simulation ends.\n");
                if(modeChooseOptions.getSelectedIndex() == 0)
                {
                    ffAdapter = new FirstFitAdapter(nowUI);
                }
                else // modeChooseOptions.getSelectedIndex() == 1
                {
                    nfAdapter = new NextFitAdapter(nowUI);
                }
                commandArrayListCnt = 0;
            }
            canvasCon.repaint();
            canvasCon.revalidate();
        });
        container.add(stepButton);
    }

    private void setGridLayoutConstraints(GridBagLayout layout)
    {
        //layout.columnWidths = new int[]{120,180,180,180,180,60,60,60,60,120};
        layout.rowHeights = new int[]{70,85,85,100,canvasHeight};
        GridBagConstraints s= new GridBagConstraints();
        s.fill = GridBagConstraints.BOTH;
        s.gridheight = 1;
        s.gridwidth = 0; //该方法是设置组件水平所占用的格子数，如果为0，就说明该组件是该行的最后一个
        s.weightx = 1; //该方法设置组件水平的拉伸幅度，如果为0就说明不拉伸，不为0就随着窗口增大进行拉伸，0到1之间
        s.weighty = 1; //该方法设置组件垂直的拉伸幅度，如果为0就说明不拉伸，不为0就随着窗口增大进行拉伸，0到1之间
        layout.setConstraints(modeChooseOptions, s); //设置组件
        s.gridheight = 3;
        s.gridwidth = 3;
        layout.setConstraints(board, s);
        s.gridheight = 1;
        s.gridwidth = 2;
        layout.setConstraints(descriptionLabel, s);
        layout.setConstraints(descriptionTextField, s);
        layout.setConstraints(applyButton, s);
        s.gridwidth = 0;
        s.gridheight = 3;
        layout.setConstraints(reset, s);
        s.gridy = 2;
        s.gridwidth = 3;
        s.gridheight = 1;
        layout.setConstraints(typeLabel, s);
        layout.setConstraints(typeComboBox, s);
        s.gridy = 3;
        layout.setConstraints(autoButton, s);
        layout.setConstraints(stepButton, s);
        s.gridy = 4;
        s.gridwidth = 0;
        s.gridheight = 0;
        s.weightx = 1;
        s.weighty = 1;
        layout.setConstraints(canvasCon, s);
    }
}
