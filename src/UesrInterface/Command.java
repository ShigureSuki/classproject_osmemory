package UesrInterface;

import java.util.HashMap;

enum CommandType { ADD, REMOVE }

class Command
{
    private boolean state; // true->apply
    private int length;
    private int tag;
    static private int tagCnt=0;
    static private HashMap<Integer,Integer> hashMap = new HashMap<>();

    Command(CommandType type, int arg)
    {
        state=(type==CommandType.ADD);
        if(state)
        {
            hashMap.put(tag=++tagCnt, length=arg);
        }
        else
        {
            length=hashMap.get(tag=arg);
        }
    }

    int getLength()
    {
        return length;
    }

    boolean getState()
    {
        return state;
    }

    int getTag()
    {
        return tag;
    }
    //TODO: Remove tag from hashMap if the command is REMOVE and it has been done.
}
